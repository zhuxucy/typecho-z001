![logo_20240628_uugai.com_1719570120470.webp](https://z6666.zhuxu.asia/2024/06/28/667e8f0dab9a6.webp)

# typecho-Z001

> 一款基于 Typecho 博客的极致优化单栏主题

QQ 交流群：151407331<br/>
Git 仓库：https://gitee.com/zhuxucy/typecho-Z001

# 如何安装？

## 环境

- PHP 8.0 以上

- MySQL, PostgreSQL, SQLite 任意一种数据库支持

- Typecho ≥ 1.2.0

- 所需Typecho插件：<br/>
  友链插件：https://chuying.lanzoue.com/i2opM20g472h<br/>
- 纯真ip库：https://chuying.lanzoue.com/iONdb20g22ud<br/>
  下载后解压至网站根目录，注意是网站根目录！！！不是主题目录！！！

## 安装Typecho

请参考其他教程

- [开始安装 - Typecho Docs](https://docs.typecho.org/install)

- [Typecho安装教程图文详解 - 30分钟轻松搭建一个博客 - VPS234主机测评](https://www.vps234.com/typecho-anzhuang-jiaocheng/)

## 安装主题

上转压缩包至网站根目录下的`/usr/themes`

![1719575498881.webp](https://z6666.zhuxu.asia/2024/06/28/667ea3e004a40.webp)

解压压缩包

![1719575721874.webp](https://z6666.zhuxu.asia/2024/06/28/667ea4aad3cff.webp)

在后台启用主题

![1719575769558.webp](https://z6666.zhuxu.asia/2024/06/28/667ea4da9f09c.webp)

接下来你将看到一个奇迹！（也可能是报错）

![1719575803855.webp](https://z6666.zhuxu.asia/2024/06/28/667ea4fcc3811.webp)

小提示：

![1719575878897.webp](https://z6666.zhuxu.asia/2024/06/28/667ea54837078.webp)

## 安装插件

进入链接下载插件：https://chuying.lanzoue.com/i2opM20g472h

与上传主题类似，只不过这次需要上传至`/usr/plugins`

![1719576236196.webp](https://z6666.zhuxu.asia/2024/06/28/667ea6ace6795.webp)

然后和主题一样进行解压

然后进入后台启用

![1719576338159.webp](https://z6666.zhuxu.asia/2024/06/28/667ea71379d4b.webp)

## 安装纯真ip库

这个更简单，进入链接下载压缩包：https://chuying.lanzoue.com/iONdb20g22ud

然后上传网站根目录，解压即可

![1719576585909.webp](https://z6666.zhuxu.asia/2024/06/28/667ea80abef25.webp)

至此，一个艺术品成功被部署在你的网站上

# 捐赠支持

一件好的艺术品需要很大的时间与精力，如果你的资金充裕，你可以选择捐赠本主题使它更加完美

|                           微信捐赠                           |                          支付宝捐赠                          |                           免费红包                           |
| :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| ![1719577394775.webp](https://z6666.zhuxu.asia/2024/06/28/667eab33ac272.webp) | ![1719577414982.webp](https://z6666.zhuxu.asia/2024/06/28/667eab4815de3.webp) | ![1719577428007.webp](https://z6666.zhuxu.asia/2024/06/28/667eab5694277.webp) |

如果你的手头不是那么充裕，你可以点击一下免费的Star，这也是对我们很大的鼓励！

![1719577068383.webp](https://z6666.zhuxu.asia/2024/06/28/667ea9ed5a7cf.webp)