<?php
if (!defined('__TYPECHO_ROOT_DIR__')) exit;
$this->need('public/header.php');
?>
<link href="<?php $this->options->themeUrl('assets/lib/prism/prism.min.css');?>" rel="stylesheet">

<div class="container">
  <div class="card p-4 respond">
    <div class="item"style="padding-bottom: 15px;border-bottom: 1px solid #000000;">
      <h6 class="text">作者：<?php $this->author(); ?></h6>
      <h6 class="text">文章：<?php $this->title() ?></h6>
      <h6 class="text">分类：<?php $this->category(','); ?></h6>
      <h6 class="text">标签：<?php $this->tags(',', true, '无'); ?></h6>
      <h6 class="text">阅读：<?php get_post_view($this) ?></h6>
      <h6 class="text">发布：<?php $this->date('Y-m-d'); ?></h6>
    </div>
    <div id="nice">
      <?php $this->content(); ?>
    </div>
    <div class="d-flex pt-4 mt-5 align-items-center border-top"></div>
  </div>
  <?php $this->need('public/comments.php'); ?>
</div>

<?php $this->need('public/footer.php'); ?>