<?php
if (!defined('__TYPECHO_ROOT_DIR__')) exit;
$this->need('public/header.php');
?>
<link href="<?php $this->options->themeUrl('assets/lib/prism/prism.min.css');?>" rel="stylesheet">

<div class="container">
  <div class="card p-4 respond">
    <div id="nice">
      <?php $this->content(); ?>
    </div>
  </div>
  <?php $this->need('public/comments.php'); ?>
</div>

<?php $this->need('public/footer.php'); ?>
