<?php
use Typecho\Widget\Helper\Form\Element\Radio;
use Typecho\Widget\Helper\Form\Element\Text;
use Typecho\Widget\Helper\Form\Element\Checkbox;
use Typecho\Widget\Helper\Form\Element\Textarea;
require_once("core/function.php");
function themeConfig($form)
{
?>
<div>
  <h2>Z001 主题设置 <?php echo _getVersion() ?></h2>
  <?php require_once('core/backup.php'); ?>
</div>
  
<?php
    $JCommentStatus = new Typecho_Widget_Helper_Form_Element_Select('JCommentStatus',array('on' => '开启（默认）','off' => '关闭'),'3','开启或关闭全站评论','介绍：用于一键开启关闭所有页面的评论 <br>注意：此处的权重优先级最高 <br>若关闭此项而文章内开启评论，评论依旧为关闭状态');
    $form->addInput($JCommentStatus->multiMode());
    /* --------------------------------------- */
    $favicon = new Typecho_Widget_Helper_Form_Element_Text('favicon', null, null, _t('站点标题 LOGO 地址'), _t('在这里填入一个图片 URL 地址, 以在网站标题前加上一个 LOGO  支持Base64 地址'));
    $form->addInput($favicon);
    /* --------------------------------------- */
    $beijing = new Typecho_Widget_Helper_Form_Element_Text('beijing', NULL, NULL, _t('网站背景'), _t('在这里输入背景链接,留空则不显示 支持Base64 地址'));
	$form->addInput($beijing);
    /* --------------------------------------- */
    $buildYear = new Typecho_Widget_Helper_Form_Element_Text('buildYear', null, date('Y'), _t('建站年份'), _t('什么时候开始建站的呀'));
    $form->addInput($buildYear);
    /* --------------------------------------- */
    $Notice = new Typecho_Widget_Helper_Form_Element_Text('Notice', NULL, NULL, _t('站点首页公告'), _t('在这里输入公告内容,留空则不显示'));
	$form->addInput($Notice);
    /* --------------------------------------- */
    $ICPbeian = new Typecho_Widget_Helper_Form_Element_Text('ICPbeian', NULL, NULL, _t('ICP备案号'), _t('在这里输入ICP备案号,留空则不显示'));
	$form->addInput($ICPbeian);
    /* --------------------------------------- */
	$gonganbeian = new Typecho_Widget_Helper_Form_Element_Text('gonganbeian', NULL, NULL, _t('公安联网备案号'), _t('在这里输入公安联网备案号,留空则不显示'));
	$form->addInput($gonganbeian);
    /* --------------------------------------- */
    $CustomCSS = new Typecho_Widget_Helper_Form_Element_Textarea('CustomCSS', NULL, NULL, _t('自定义样式'), _t('在这里填入你的自定义样式（直接填入css，无需&lt;style&gt;标签）'));
	$form->addInput($CustomCSS);
    /* --------------------------------------- */
	$CustomContent = new Typecho_Widget_Helper_Form_Element_Textarea('CustomContent', NULL, NULL, _t('底部自定义内容'), _t('位于底部，footer之后body之前，适合放置一些JS内容，如网站统计代码等（若开启全站Pjax，目前支持Google和百度统计的回调，其余统计代码可能会不准确）'));
	$form->addInput($CustomContent);
}
