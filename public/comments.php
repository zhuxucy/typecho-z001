<style type="text/css">
#comments {
    padding-top:15px;
    margin:0;
}
.comment-list,.comment-list ol {
    list-style:none;
    margin:0;
    padding:0;
}
.comment-list li {
    border-radius:8px;
    padding:15px;
    margin-top:15px;
}
.comment-list p {
    padding:10px 0;
    margin:10px 0 0 0;
}
.comment-list li.comment-parent {
    border:1px solid #eee;
}
.comment-list li.comment-parent .comment-content {
    background:#f6f6f6;
    border-radius:8px;
    margin-top:25px;
    padding:10px;
    position:relative;
    word-break:break-word;
    word-wrap:break-word;
}
.comment-list li.comment-parent .comment-content p {
    padding:0;
    margin:0;
}
.comment-list li.comment-parent .comment-content::before {
    content:'';
    width:0;
    height:0;
    border:10px solid transparent;
    border-bottom-color:#f6f6f6;
    position:absolute;
    top:0;
    left:15px;
    margin-top:-20px;
}
.comment-list li.comment-level-1 {
    background:#f6f6f6;
    border:1px solid #eee;
}
.comment-list li.comment-level-1 .comment-content {
    background:#FFF;
    border-radius:8px;
    margin-top:25px;
    padding:10px;
    position:relative;
    word-break:break-word;
    word-wrap:break-word;
}
.comment-list li.comment-level-1 .comment-content p {
    padding:0;
    margin:0;
}
.comment-list li.comment-level-1 .comment-content::before {
    content:'';
    width:0;
    height:0;
    border:10px solid transparent;
    border-bottom-color:#FFF;
    position:absolute;
    top:0;
    left:15px;
    margin-top:-20px;
}
.comment-list li.comment-level-2 {
    background:#FFF;
    border:1px solid #eee;
}
.comment-list li.comment-level-2 .comment-content {
    background:#f6f6f6;
    border-radius:8px;
    margin-top:25px;
    padding:10px;
    position:relative;
    word-break:break-word;
    word-wrap:break-word;
}
.comment-list li.comment-level-2 .comment-content p {
    padding:0;
    margin:0;
}
.comment-list li.comment-level-2 .comment-content::before {
    content:'';
    width:0;
    height:0;
    border:10px solid transparent;
    border-bottom-color:#f6f6f6;
    position:absolute;
    top:0;
    left:15px;
    margin-top:-20px;
}
.comment-list li.comment-level-3 {
    background:#f6f6f6;
    border:1px solid #eee;
}
.comment-list li.comment-level-3 .comment-content {
    background:#FFF;
    border-radius:8px;
    margin-top:25px;
    padding:10px;
    position:relative;
    word-break:break-word;
    word-wrap:break-word;
}
.comment-list li.comment-level-3 .comment-content p {
    padding:0;
    margin:0;
}
.comment-list li.comment-level-3 .comment-content::before {
    content:'';
    width:0;
    height:0;
    border:10px solid transparent;
    border-bottom-color:#FFF;
    position:absolute;
    top:0;
    left:15px;
    margin-top:-20px;
}
.comment-list li.comment-level-4 {
    background:#FFF;
    border:1px solid #eee;
}
.comment-list li.comment-level-4 .comment-content {
    background:#f6f6f6;
    border-radius:8px;
    margin-top:25px;
    padding:10px;
    position:relative;
    word-break:break-word;
    word-wrap:break-word;
}
.comment-list li.comment-level-4 .comment-content p {
    padding:0;
    margin:0;
}
.comment-list li.comment-level-4 .comment-content::before {
    content:'';
    width:0;
    height:0;
    border:10px solid transparent;
    border-bottom-color:#f6f6f6;
    position:absolute;
    top:0;
    left:15px;
    margin-top:-20px;
}
.comment-list li.comment-level-5 {
    background:#f6f6f6;
    border:1px solid #eee;
}
.comment-list li.comment-level-5 .comment-content {
    background:#FFF;
    border-radius:8px;
    margin-top:25px;
    padding:10px;
    position:relative;
    word-break:break-word;
    word-wrap:break-word;
}
.comment-list li.comment-level-5 .comment-content p {
    padding:0;
    margin:0;
}
.comment-list li.comment-level-5 .comment-content::before {
    content:'';
    width:0;
    height:0;
    border:10px solid transparent;
    border-bottom-color:#FFF;
    position:absolute;
    top:0;
    left:15px;
    margin-top:-20px;
}
.comment-list li.comment-level-6 {
    background:#FFF;
    border:1px solid #eee;
}
.comment-list li.comment-level-6 .comment-content {
    background:#f6f6f6;
    border-radius:8px;
    margin-top:25px;
    padding:10px;
    position:relative;
    word-break:break-word;
    word-wrap:break-word;
}
.comment-list li.comment-level-6 .comment-content p {
    padding:0;
    margin:0;
}
.comment-list li.comment-level-6 .comment-content::before {
    content:'';
    width:0;
    height:0;
    border:10px solid transparent;
    border-bottom-color:#f6f6f6;
    position:absolute;
    top:0;
    left:15px;
    margin-top:-20px;
}
.comment-list li.comment-level-odd {
    border:1px solid #eee;
}
.comment-list li.comment-level-even {
    background:#fff;
    border:1px solid #eee;
}
.comment-list li.comment-by-author {
    border:1px dashed #f4792c;
}
.comment-list li .comment-reply {
    font-size:0.87rem;
    text-align:right;
}
.comment-meta {
    margin:0;
    color:#999;
}
.comment-awaiting-moderation {
    color:#999;
}
.comment-author {
    display:block;
    margin:0;
}
.comment-author .avatar {
    border-radius:5px;
    float:left;
    margin-right:15px;
}
.comment-author cite {
    font-weight:600;
    font-style:normal;
}
.comment-list .respond {
    margin-top:15px;
    border-top:1px solid #eee;
}
.respond .cancel-comment-reply {
    float:right;
    margin-top:15px;
    font-size:0.87rem;
}
#comment-form label {
    display:block;
    margin-bottom:.5em;
    font-weight:bold;
}
#comment-form .required:after {
    content:" *";
    color:#666;
}
       
@media screen and (max-width: 767px) {
#comments .comment-list>.comment-body>.comment-children {
  margin-left:16px
}
}
    
@media (min-width: 768px){
.inputgrap {
  display: grid;
  grid-template-columns: repeat(3, minmax(0, 1fr));
  gap: 1rem;
}
    
.owner {
  flex-shrink: 0;
  background: #a86af9;
  color: #fff;
  padding: 0 5px;
  border-radius: 2px;
  font-style: normal;
}
    
.tk-extra {
  background: #ffffff;
  border: 1px solid #dee3f3;
  padding: 1px 5px 1px 2px;
  border-radius: 2px;
  margin-right: 4px!important;
  color: rgb(63 63 70 / 80%)!important;
  display: inline!important;
  margin-top: 6px!important;
}
</style>

<?php $this->comments()->to($comments); ?>

<div class="card p-4 respond" style="margin-top:15px">
    <?php if ($this->hidden) : ?>
      <span>当前文章受密码保护，无法评论</span>
    <?php else : ?>
    <?php if ($this->allow('comment') && $this->options->JCommentStatus !== "off") : ?>
      <h3 style="font-size: 18px;font-weight: bold;">添加评论（有大BUG）</h3>
      
      <div id="<?php $this->respondId(); ?>">
        <?php $comments->cancelReply(); ?>  
        
        <form method="post" action="<?php $this->commentUrl() ?>" id="comment-form" role="form">
          <div class="inputgrap">
            <p>
                <label for="author" class="required" style="color: #3F51B5;font-size: small;"><?php _e('昵称'); ?></label>
                <input class="form-control" type="text" name="author" id="author" class="text" placeholder="必填"  value="<?php $this->user->hasLogin() ? $this->user->screenName() : $this->remember('author') ?>"autocomplete="off" maxlength="16" required/>
              </p>
            <p>
                <label style="color: #3F51B5;font-size: small;" for="mail" class="required" <?php if ($this->options->commentsRequireMail): ?> <?php endif; ?> ><?php _e('邮箱'); ?></label>
                <input class="form-control" type="email" name="mail" id="mail" class="text" placeholder="必填" value="<?php $this->user->hasLogin() ? $this->user->mail() : $this->remember('mail') ?>"<?php if ($this->options->commentsRequireMail): ?> autocomplete="off" required<?php endif; ?> />
              </p>
            <p>
                <label style="color: #3F51B5;font-size: small;" for="url"<?php if ($this->options->commentsRequireURL): ?> class="required"<?php endif; ?>><?php _e('网址'); ?></label>
                <input class="form-control" type="url" name="url" id="url" autocomplete="off" class="text" placeholder="<?php _e('https://'); ?>" value="<?php $this->remember('url'); ?>"<?php if ($this->options->commentsRequireURL): ?> required<?php endif; ?> />
              </p>
          </div>
          <div class="mb-3">
            <label style="color: #3F51B5;font-size: small;" for="textarea" class="required"><?php _e('内容'); ?></label>
            <textarea class="form-control OwO-textarea" rows="8" cols="50" name="text" id="textarea" class="textarea" placeholder="善语结善缘，恶语伤人心..."required><?php $this->remember('text'); ?></textarea>
            <div class="OwO"></div>
          </div>
          <div class="d-grid">
            <button class="btn btn-outline-secondary btn-block btn-sm" type="submit"style="float: right;">发送评论</button>
          </div>
        </form>
      </div>
      
      <h3 class="comment-separator">
        <div class="comment-tab-current">
          <div style="margin: 20px auto;width: fit-content;">
            ----------- 
            <span style="color: white;background-color: black;padding: 0 5px;font-size: .7rem;">
              <?php $this->commentsNum(_t('暂无评论'), _t('仅有一条评论'), _t('已有 %d 条评论')); ?>
            </span> 
            -----------
          </div>
        </div>
      </h3>  
      
      <?php if ($comments->have()) : ?>
      <!--<div class="card p-4 respond">-->
        <?php $comments->listComments(); ?>
      <!--</div>-->
      <?php endif; ?>
      
      <?php else : ?>
        <?php if ($this->options->JCommentStatus === "off") : ?>
          <span>博主关闭了所有页面的评论</span>
        <?php else : ?>
          <span>博主关闭了当前页面的评论</span>
        <?php endif; ?>
      <?php endif; ?>
    <?php endif; ?>
</div>


<?php function threadedComments($comments, $options) {
    $commentClass = '';
    if ($comments->authorId) {
        if ($comments->authorId == $comments->ownerId) {
            $commentClass .= ' comment-by-author';
        } else {
            $commentClass .= ' comment-by-user';
        }
    }

    $commentLevelClass = $comments->levels > 0 ? ' comment-child' : ' comment-parent';
?>

<li id="li-<?php $comments->theId(); ?>" class="comment-body<?php 
if ($comments->levels > 0) {
    echo ' comment-child';
    $comments->levelsAlt(' comment-level-odd', ' comment-level-even');
} else {
    echo ' comment-parent';
}
$comments->alt(' comment-odd', ' comment-even');
echo $commentClass;
?>">
    <div id="<?php $comments->theId(); ?>">
        <div class="comment-author">
            <?php $comments->gravatar('40', ''); ?>
            <cite class="fn"><?php $comments->author(); ?></cite>
        </div>
        <div class="comment-meta">
            <a href="<?php $comments->permalink(); ?>"><?php $comments->date('Y-m-d H:i'); ?></a>
            <span class="comment-reply"><?php $comments->reply(); ?></span>
        </div>
        <?php $comments->content(); ?>
        <span class="badge rounded-pill bg-danger"><?php echo convertip($comments->ip); ?></span>
        <span class="badge rounded-pill bg-primary"><?php _getAgentOS($comments->agent); ?></span>
        <span class="badge rounded-pill bg-success"><?php _getAgentBrowser($comments->agent); ?></span>
    </div>
<?php if ($comments->children) { ?>
    <div class="comment-children">
        <?php $comments->threadedComments($options); ?>
    </div>
<?php } ?>
</li>
<?php } ?>


<script>
  var OwO_demo = new OwO({
    logo: 'OωO',
    container: document.getElementsByClassName('OwO')[0],
    target: document.getElementsByClassName('OwO-textarea')[0],
    api: '<?php $this->options->themeUrl('OwO.json'); ?>',
    position: 'down',
    width: '100%',
    maxHeight: '250px'
  });
</script>