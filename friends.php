<?php

/**
 * 友链
 * 
 * @package custom 
 * 
 **/
if (!defined('__TYPECHO_ROOT_DIR__')) exit;
$this->need('public/header.php');
?>
<div class="container">
  <div class="card p-4 respond">
    <div id="nice">
       <?php $this->content(); ?>
    </div>
    <div id="link-list">
      <?php if (isset($this->options->plugins['activated']['Links'])) : ?>
        <?php
          Links_Plugin::output('
          
          <li><a href="{url}" title="{title}" target="_blank" rel="noopener">{name}：{url}</a></li>
          
		  ', 0);
        ?>
      <?php endif; ?>
  </div>
  </div>
<?php $this->need('public/comments.php'); ?>
</div>
<?php $this->need('public/footer.php'); ?>